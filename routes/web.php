<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PageController@home');

Route::get('/new-chat', 'PageController@newChat');

Route::get('/archive', 'PageController@archive');

Route::get('/archive/1', 'PageController@archive1');
Route::get('/archive/2', 'PageController@archive2');
Route::get('/archive/3', 'PageController@archive3');
Route::get('/archive/4', 'PageController@archive4');
Route::get('/archive/5', 'PageController@archive5');

Route::get('/profile', 'PageController@profile');

Route::get('set-jwt', 'chatController@setJwtToSession'); //get new every 10 mins
Route::post('text-to-voice', 'chatController@textToVoice');
