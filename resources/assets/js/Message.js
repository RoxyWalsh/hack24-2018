var moment = require('moment');

class Message {

    constructor() {
        this.date = new Date();
        this.hours = '';
        this.minutes = '';
        this.text = '';
        this.type = '';
        this.audioClip = '';
    }

    setDates() {
        this.hours = moment(this.date).format('HH');
        this.minutes = moment(this.date).format('mm');
    }

}

export default Message;