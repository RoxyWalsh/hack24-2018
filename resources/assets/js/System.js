var moment = require('moment');
import Message from './Message';

class System {

    constructor() {
        this.showStart = true;
        this.showInput = false;
        this.showEnd = false;
        this.recipient = "";
        this.messages = [];
        this.message = new Message();
        this.errors = [];
        this.lastMessageTime = null;



        this.defaultFakeMessages = [
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Hello yes it is me who are you',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Batman something must be urgent for you to call',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Careful of what what are you talking about',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Shit I have already eaten 4',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Ok but what is wrong with the donuts',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Oh god no',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'My stomach does not feel great',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'It is too late for me Batman',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Tell my wife splat splat oh god splat help me splat splat splat',
                type: 'reply',
                audioClip: '',
            },
        ];

        this.pikaFakeMessages = [
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Pika Pika??',
                type: 'reply',
                audioClip: '',
            }
        ];

        this.droidsFakeMessages = [
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: '',
                type: 'reply',
                audioClip: '',
            },
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: '',
                type: 'reply',
                audioClip: '',
            }
        ];

        this.rickrollFakeMessages = [
            {
                date: moment(this.message.date),
                hours: moment(this.message.date).format('HH'),
                minutes: moment(this.message.date).format('mm'),
                text: 'Never gonna let you down',
                type: 'reply',
                audioClip: '',
            }
        ];

         this.fakeMessages = this.defaultFakeMessages;
        // this.fakeMessages = this.pikaFakeMessages;
        //this.fakeMessages = this.droidsFakeMessages;
        // this.fakeMessages = this.rickrollFakeMessages;

        //add any needed variables here
        this.fetchHeaders = {
            "Content-Type": "application/json",
            "X-Request-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.head.querySelector('meta[name="csrf-token"]').content,
            "Accept": "application/json"
        }
    }


    toggleStart() {
        this.showInput = !this.showInput;
        this.showStart = !this.showStart;
    }

    toggleEnd() {
        this.showInput = !this.showInput;
        this.showEnd = !this.showEnd;
    }

    setMessage(message) {
        this.message.text = message;

    }

    showTime(date) {

        if (this.messages.length < 1) {
            return true;
        }

        let lastMessage = this.messages[(this.messages.length - 1)];

        this.lastMessageTime = moment(lastMessage.date);

        this.currentMessageTime = moment(date);

        //get last two messages, compare return true or false
        let time = this.currentMessageTime.diff(this.lastMessageTime, 'minutes');

        console.log(this.lastMessageTime, this.currentMessageTime, time);

        if (time > 0) {
            return true;
        }

        return false;
    }

    sendTextToApi() {
        //send this.message to the api endpoint (our controller)
        //get audio file back from the api, save to disk and returns location of file
        //TODO make sure we have the jwt token to send to the api

        let postUrl = '/text-to-voice';

        let thisSystem = this;

        fetch(postUrl, {
            method: 'post',
            headers: this.fetchHeaders,
            credentials: 'same-origin',
            body: JSON.stringify({message: this.message.text}),
        }).then(function (response) {
            return response.json()
        }).then(function (json) {
            //now call the function to autoplay the audio file
            thisSystem.message.audioClip = json.audioFile;
            thisSystem.playAudio(thisSystem.message.audioClip);
        })
    }

    playAudio(path = this.message.audioClip) {
        console.log('playing');
        let audio = new Audio(path);
        audio.play();
    }

    easterEggs(){
        console.log(this.recipient);
        console.log(this.message.text);

        if(this.recipient == 151 && this.message.text == 'skin:pikachu'){
            $('#app').addClass('skin-pikachu');
        }

        if(this.recipient == 405 && this.message.text == "These aren't the droids you're looking for."){
            console.log('droids');
           this.message.text = "These aren't the droids we're looking for";
            // this.sendTextToApi()
        }

        if(this.recipient == 405 && this.message.text == "He can go about his business."){
            console.log('droids');
            this.message.text = "You can go about your business.";
        }

        if(this.recipient == 405 && this.message.text == "Move along."){
            console.log('droids');
            this.message.text = "Move along... move along.";
        }

        if(this.recipient == 2008 && this.message.text == "Never gonna give you up"){
            console.log('rick');

            var thisSys = this;

            setTimeout(function(){
                thisSys.playAudio('eggs/rickroll.mp3');
            }, 6000);
        }
    }

    // jumpToLastPostOnLoad() {
    //     let messages = document.querySelector('.messages');
    //     messages.scrollTop = messages.scrollHeight;
    // }
    //
    //
    //
    // resizeFeedBody() {
    //     // console.log('resizeFeedBody');
    //
    //     if (document.querySelector('.feed-body')) {
    //         let feedHeader = document.querySelector('.feed-header');
    //         let feedBody = document.querySelector('.feed-body');
    //         let feedFooter = document.querySelector('.feed-footer');
    //
    //         let feedContainer = document.querySelector('.feed-container');
    //
    //         // console.log('footer height ' + feedFooter.offsetHeight);
    //         feedBody.style.height = feedContainer.offsetHeight - (feedHeader.offsetHeight + feedFooter.offsetHeight) + "px";
    //     }
    //
    // }
    //
    // resizePageBody() {
    //     // console.log('resizePageBody');
    //
    //     if (document.querySelector('.app-page-header')) {
    //         let appPageHeader = document.querySelector('.app-page-header');
    //         let pageBody = document.querySelector('.page-body');
    //
    //         pageBody.style.height = "calc(100% - " + appPageHeader.offsetHeight + "px)";
    //     }
    //
    // }

    scrollToLastPost() {

        console.log('scrollToLastPost');

        if (document.querySelector('.messages')) {
            let feed = document.querySelector('.chat-feed');
            const messages = document.querySelector('.messages');
            const allPosts = messages.querySelectorAll('.message');
            let lastPost = allPosts[allPosts.length - 1];

            if (!lastPost) {
                lastPost = 0;
                console.log('not last message', lastPost);
            }

            console.log('move', messages.scrollHeight, messages.scrollTop, messages.offsetHeight, lastPost.offsetHeight);

            // scroll if user hasn't navigated away from the bottom
            // if (Math.abs(parseInt((messages.scrollHeight - messages.scrollTop)) - messages.offsetHeight) < (lastPost.offsetHeight + 47)) {
                console.log('not firing');
                $('.messages').stop().animate({scrollTop: messages.scrollHeight}, 10, 'linear');
            // }
        }
    }

    scrollToQuestion(id) {
        const question = document.querySelector('[data-name="' + id + '"]');
        const messages = document.querySelector('.messages');

        const feedHeader = document.querySelector('.feed-header');

        if (question.offsetTop !== messages.scrollTop) {
            $(messages).stop().animate({scrollTop: (question.offsetTop - feedHeader.offsetHeight)}, this.scrollSpeed, 'linear');
        }

    }


}

export default System;