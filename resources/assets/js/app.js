
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import System from './System';

window.Vue = require('vue');

window.system = new System();

Vue.prototype.$system = window.system;

console.log(window.system);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chat-wrap', require('./components/chat-wrap.vue'));
Vue.component('chat-nav', require('./components/chat-nav.vue'));
Vue.component('chat-start', require('./components/chat-start.vue'));
Vue.component('chat-end', require('./components/chat-end.vue'));
Vue.component('chat-input', require('./components/chat-input.vue'));
Vue.component('chat-feed', require('./components/chat-feed.vue'));

const app = new Vue({
    el: '#app',
    system,
});
