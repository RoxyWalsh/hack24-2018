@extends('layout')

@section('title', 'Page Title')


@section('content')
    <div class="content content--archive">
        <h2>Call archive</h2>

        <ul class="archive">
            <li class="archive-item">
                <a href="/archive/5">
                    <h6>07539 017357</h6>
                    <p><span class="archive__time">12:37</span> - <span class="archive__date">11/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/4">
                    <h6>07700 900581</h6>
                    <p><span class="archive__time">10:00</span> - <span class="archive__date">11/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/3">
                    <h6>07700 900139</h6>
                    <p><span class="archive__time">08:19</span> - <span class="archive__date">11/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/2">
                    <h6>07539 017357</h6>
                    <p><span class="archive__time">01:46</span> - <span class="archive__date">11/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/1">
                    <h6>7700 900319</h6>
                    <p><span class="archive__time">01:13</span> - <span class="archive__date">11/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/1">
                    <h6>7700 900565</h6>
                    <p><span class="archive__time">20:58</span> - <span class="archive__date">10/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/1">
                    <h6>7700 900874</h6>
                    <p><span class="archive__time">15:23</span> - <span class="archive__date">10/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
            <li class="archive-item">
                <a href="/archive/1">
                    <h6>07700 900581</h6>
                    <p><span class="archive__time">13:31</span> - <span class="archive__date">10/03/2018</span></p>
                    <span class="archive-item__info"><i class="fas fa-info"></i></span>
                </a>
            </li>
        </ul>
    </div>

    <div class="navigation navigation--links">
        <a href="/" class="navigation__item">
            <i class="fas fa-home"></i>
        </a>
        <a href="/new-chat" class="navigation__start">
            <i class="fas fa-comment-alt"></i>
        </a>
        <a href="/profile" class="navigation__item">
            <i class="far fa-user"></i>
        </a>
    </div>
@endsection