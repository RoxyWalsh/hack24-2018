@extends('layout')

@section('title', 'Page Title')


@section('content')
    <div class="content content--archive content--archive-detail">
        <div class="archive-item">
            <a href="/archive/5">
                <h6>07539 017357</h6>
                <p><span class="archive__time">12:37</span> - <span class="archive__date">11/03/2018</span></p>
            </a>
        </div>

        <ul class="messages">

            <li class="message message--sent">
                <span class="message__time">12:37</span>
                Gordon, is that you?
            </li>

            <li class="message message--reply">
                Hello yes it is me who are you
            </li>

            <li class="message message--sent">
                I'm the Batman
            </li>

            <li class="message message--reply">
                Batman something must be urgent for you to call
            </li>

            <li class="message message--sent">
                Gordon, you need to be careful
            </li>

            <li class="message message--reply">
                Careful of what what are you talking about
            </li>

            <li class="message message--sent">
                The donuts have been compromised, donut eat them!
            </li>

            <li class="message message--reply">
                Shit I have already eaten 4
            </li>

            <li class="message message--sent">
                <span class="message__time">12:38</span>
                I'll be there in 3 minutes
            </li>

            <li class="message message--reply">
                Ok but what is wrong with the donuts
            </li>

            <li class="message message--sent">
                The Joker, he filled them with laxatives
            </li>

            <li class="message message--reply">
                Oh god no
            </li>

            <li class="message message--sent">
                You need to get to a toilet or this could get real messy
            </li>

            <li class="message message--reply">
                My stomach does not feel great
            </li>

            <li class="message message--sent">
                Get to a toilet Gordon! Quickly
            </li>

            <li class="message message--reply">
                It is too late for me Batman
            </li>

            <li class="message message--sent">
                No Gordon!
            </li>

            <li class="message message--reply">
                Tell my wife splat splat oh god splat help me splat splat splat
            </li>

            <li class="message message--sent">
                Gordon! Noooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo!
            </li>

            <li class="message message--end">Call ended</li>
        </ul>
    </div>

    <div class="navigation navigation--links">
        <a href="/archive" class="navigation__item">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a href="/new-chat" class="navigation__start">
            <i class="fas fa-comment-alt"></i>
        </a>
        <a href="/profile" class="navigation__item">
            <i class="far fa-user"></i>
        </a>
    </div>
@endsection