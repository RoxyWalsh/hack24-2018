@extends('layout')

@section('title', 'Page Title')


@section('content')
    <div class="content content--profile">
        <h2>Your profile</h2>

        <div class="form-group">
            <label for="name">Name</label>
            <input id="name" type="text" value="Bruce Wayne">
        </div>

        <div class="form-group">
            <label for="phoneNum">My number</label>
            <input id="phoneNum" type="tel" placeholder="07700 900650" disabled>
        </div>

        <div class="form-group">
            <label for="voicePref">Voice preference</label>
            <select name="voicePref" id="voicePref" id="">
                <option value="male">Male</option>
                <option selected="selected" value="femlae">Female</option>
            </select>
        </div>

        <div class="form-group">
            <button>Save changes</button>
        </div>
    </div>

    <div class="navigation navigation--links">
        <a href="/archive" class="navigation__item">
            <i class="far fa-comments"></i>
        </a>
        <a href="/new-chat" class="navigation__start">
            <i class="fas fa-comment-alt"></i>
        </a>
        <a href="/" class="navigation__item">
            <i class="fas fa-home"></i>
        </a>
    </div>
@endsection