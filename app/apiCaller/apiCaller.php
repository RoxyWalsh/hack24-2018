<?php
/**
 * Created by PhpStorm.
 * User: Roxy
 * Date: 10/03/2018
 * Time: 13:01
 */

namespace App\apiCaller;

use GuzzleHttp\Client;



class apiCaller
{

    //use to make request for JWT
    private $key = '4188d9e715b14fa8836984bf15378bd2';

    private $urlGetAuthToken = 'https://api.cognitive.microsoft.com/sts/v1.0/';
///issueToken
///
    private $uriTextToSpeech = 'https://speech.platform.bing.com/';

    public function getApiToken()
    {

    }


    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * generate a token with which to connect to the api
     */
    public function getJwtToken()
    {
        $client = new Client([
            'base_uri' => $this->urlGetAuthToken,
            'verify' => false,
        ]);

        $postRequest = [
            'headers' => [
                'Ocp-Apim-Subscription-Key' => $this->key,
            ]
        ];

        $response = $client->request('POST', 'issueToken', $postRequest);

        $body = $response->getBody();
        $stringBody = (string) $body;

        $jwtToken = $stringBody;

        return $jwtToken;
    }

    public function sendTextToApi($message, $key = null)
    {
        $jwtToken = session('jwtToken');

//        $message = 'test message';
        $gender = 'Male';
        $genderName = 'Susan, Apollo'; //female
//        $genderName = 'George, Apollo'; //male

        $client = new Client([
            'base_uri' => $this->uriTextToSpeech,
            'verify' => false,
        ]);

        $postRequest = [
            'headers' => [
                'X-Microsoft-OutputFormat' => 'audio-16khz-32kbitrate-mono-mp3',
                'Content-Type' => 'application/ssml+xml',
                'Host' => 'speech.platform.bing.com',
                'Authorization' => 'Bearer ' . $jwtToken,
            ],
            'body' => '<speak version=\'1.0\' xml:lang=\'en-GB\'><voice xml:lang=\'en-GB\' xml:gender=\'' . $gender .'\' name=\'Microsoft Server Speech Text to Speech Voice (en-GB, ' . $genderName . ')\'>' . $message . '</voice></speak>'
        ];

        $response = $client->request('POST', 'synthesize', $postRequest);
//        dd($postRequest);

        return $response;
    }
}

