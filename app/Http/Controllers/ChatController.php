<?php

namespace App\Http\Controllers;

use App\apiCaller\apiCaller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class ChatController extends Controller
{
    protected $apiCaller;

    public function __construct(apiCaller $apiCaller)
    {
        $this->apiCaller = $apiCaller;
    }

    public function setJwtToSession(){
        $jwtToken = $this->apiCaller->getJwtToken();

        //save the token to the session
        session([
            'jwtToken' => $jwtToken,
        ]);
    }


    public function textToVoice(Request $request){

        $message = $request->get('message');

        $speechResponse = $this->apiCaller->sendTextToApi($message);

        $speechResponseBody = $speechResponse->getBody();

        $speechResponseDir = 'speech-responses/';

        $newFilename = hash('md5', uniqid(mt_rand(), true), false);

        $fullFilePath = $speechResponseDir . $newFilename . '.mp3';

//        $speechFile = Storage::put( $speechResponseDir . $newFilename . '.mp3', $speechResponseBody);

        $speechFile = file_put_contents($fullFilePath, $speechResponseBody);

        return response()->json([
            'success' => 'true',
            'message' => 'Text converted to audio',
            'audioFile' => $fullFilePath,
        ]);


    }
}
