<?php

namespace App\Http\Controllers;

use App\apiCaller\apiCaller;
use Illuminate\Http\Request;

class PageController extends Controller
{

    protected $apiCaller;

    public function __construct(apiCaller $apiCaller)
    {
        $this->apiCaller = $apiCaller;
    }

    public function home(){

        $jwtToken = $this->apiCaller->getJwtToken();

        //save the token to the session
        session([
            'jwtToken' => $jwtToken,
        ]);

        return view('home');
    }

    public function newChat(){

        return view('newChat');
    }

    public function archive(){

        return view('archive.index');
    }

    public function archive1(){

        return view('archive.1');
    }

    public function archive2(){

        return view('archive.2');
    }

    public function archive3(){

        return view('archive.3');
    }

    public function archive4(){

        return view('archive.4');
    }

    public function archive5(){

        return view('archive.5');
    }

    public function profile(){

        return view('profile');
    }
}
